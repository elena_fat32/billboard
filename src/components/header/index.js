import React, { Component } from 'react';
import {render} from 'react-dom';
import {Route, Switch, Link, withRouter } from 'react-router-dom';
import './style.css';

class Header extends Component {
    render() {
        return (
            <header>
                <Link className="btn header-btn" to="/">Go home</Link>
                <Link className="btn btn-simple header-btn" to="/new">New ad</Link>
            </header>
    )
    }
}

export default Header;
