import React, {Component} from 'react';

import './style.css';

var validForm = {
    title: true,
    phone: true,
    description: true
};

class EditAd extends Component {
    constructor(props) {
        super(props);
        var id = props.match.params.id,
            detail =  {},
            mode = "";

        if (id && window.location.hash.indexOf('edit') + 1 ) {
            mode = "edit";
            detail =  this.getDetails(id);
        } else {
            mode = "new";
        }

        this.state = {
            mode: mode,
            description: detail.description || "",
            title: detail.title || "",
            phone: detail.phone || ""
        };
    }

    validateFields(name, max, req) {
        let value = this.state[name];
        var valid = true;

        if (value.length > max || req && !value.length) {
            valid = false;
        } else {
            valid = true;
        }

        validForm[name] = valid;
        this.viewOrHideError(name);
    }

    viewOrHideError(name) {
        var element = this.refs[name],
            errorElement = this.refs.error;

        if (!validForm[name])  {
            element.classList.add("error");

            let output = "Error " + name + ".";
            errorElement.innerHTML += " " + output;
        } else {
            element.classList.remove("error");
        }
    }

    validPhone(num) {
        let template = /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/;
        validForm.phone = template.test(num);

        this.viewOrHideError("phone");
    }

    validateForm() {
        this.clearErrorText();

        this.validateFields("title", 100, true);
        this.validPhone(this.state.phone);
        this.validateFields("description", 300);

        if (!validForm.title || !validForm.phone || !validForm.description ) {
            return;
        } else {
            this.saveDetail();
        }
    }

    clearErrorText() {
        this.refs.error.innerHTML = "";
    }

    getDetails(index) {
        let ads = JSON.parse(localStorage.getItem("ads"));

        return ads[parseInt(index)];
    }

    saveDetail() {
        let ads = JSON.parse(localStorage.getItem("ads"));
        var index = parseInt(this.props.match.params.id);

        if (!index) {
            index = ads.length;
            ads.push({});
            ads[index].id = index;
        }

        ads[index].title = this.state.title;
        ads[index].description = this.state.description;
        ads[index].phone = this.state.phone;
        localStorage.setItem("ads", JSON.stringify(ads));

        this.props.history.goBack();
    }

    handleChange(event) {
        var ref = event._targetInst.ref && event._targetInst.ref._stringRef || "";

        this.setState({
            [ref]: event.target.value
        });
    }

    render() {
        let modeText = this.state.mode == "edit"? "Edit ad": "Create new ad";

        return (
            <form className="edit">
                <h1>{modeText}</h1>
                <div className="edit-title">
                    <input className="edit-title-input"
                           type="text"
                           placeholder="title"
                           required="required"
                           maxLength="100"
                           ref='title'
                           defaultValue={this.state.title}
                           onChange={this.handleChange.bind(this)}/>
                </div>
                <div className="edit-phone">
                    <input className="edit-title-input"
                           type="text"
                           placeholder="russian phone number"
                           required="required"
                           ref='phone'
                           defaultValue={this.state.phone}
                           onChange={this.handleChange.bind(this)}/>
                </div>
                <div className="edit-description" ref='description'>
                    <textarea className="edit-description-textarea"
                              placeholder="description"
                              maxLength="300"
                              defaultValue={this.state.description}
                              onChange={this.handleChange.bind(this)}>
                    </textarea>
                </div>

                <div className="edit-error" ref="error"></div>
                <button className="btn btn-simple" onClick={this.validateForm.bind(this)} >Save</button>
            </form>
        )
    }
}

export default EditAd;
