import React, { Component } from 'react';

class NotFound extends Component {
    render() {
        return (
            <div className='container'>
                 Страница не найдена.
            </div>
        )
    }
}

export default NotFound;
