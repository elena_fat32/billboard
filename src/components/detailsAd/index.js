import React, {Component} from 'react';
import {Link} from 'react-router-dom';

import './style.css';

class DetailsAd extends Component {
    constructor(props) {
        super(props);

        this.state = {
            index: null,
            detail: ""
        };
    }

    componentWillMount() {
        let index = this.props.match.params.id;

        this.setState({
            index: index,
            detail:  this.getDetails(index)
        });

        this.onDelEvent = () => {
            let ads = JSON.parse(localStorage.getItem("ads"));
            ads.splice(this.state.index, 1);
            localStorage.setItem("ads", JSON.stringify(ads));

            this.props.history.goBack();
        };
    }

    getDetails(index) {
        let ads = JSON.parse(localStorage.getItem("ads"));

        return ads[parseInt(index)];
    }

    render() {
        let detail = this.state.detail;

        if (!detail) {
            return <div>Sorry, but the detail was not found</div>;
        }

        return (
            <div className="detail">
                <div className="detail-title">
                    {detail.title}
                    <Link className="btn btn-simple title-btn" to={`/edit/${this.state.index}`}>
                        Edit
                    </Link>
                    <button className="btn btn-danger title-btn"
                            type="button"
                            onClick={this.onDelEvent}>
                            Delete
                    </button>
                </div>
                <div className="detail-phone">
                    {detail.phone}
                </div>
                <div className="detail-description">
                    {detail.description}
                </div>
            </div>
        )
    }
}

export default DetailsAd;