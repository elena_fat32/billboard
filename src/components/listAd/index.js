import React, {Component} from 'react';
import {Link} from 'react-router-dom';

import "./style.css"

const ads = [
    { id: 0, title: "Ad about house", description: "I’m looking for a Spanish language conversation and prononciation course in Rzeszow. Hence, if there are any native Spanish teachers who organize such types of courses, please give me a call under the following number: 06 773 57 56. I’m ready to pay up to 120$ monthly and would be interesed in starting the classes immediately.", phone: "+7900000" },
    { id: 1, title: "Ad about big family with many dogs", description: "Yesterday I lost my book about history. It was very important for me and I hope that someone has found it.It is very thick (about 800 pages) and black. The title is “History of the Europe”. I promise a reward for the finder.You can call me.", phone: "+7900000" },
    { id: 2, title: "Ad about Sam Sweeper", description: "Thank you for your help!", phone: "+7900000" },
    { id: 3, title: "Ad about Matt Midfielder", description: "Thank you for your help!", phone: "+7900000" },
    { id: 4, title: "Ad about William Winger", description: "I’m looking for a Spanish language conversation and prononciation course in Rzeszow. Hence, if there are any native Spanish teachers who organize such types of courses, please give me a call under the following number: 06 773 57 56. I’m ready to pay up to 120$ monthly and would be interesed in starting the classes immediately.", phone: "+7900000"},
    { id: 5, title: "Ad about Fillipe Forward", description: "Thank you for your help!", phone: "+7900000" }
];

class ListAd extends Component {
    constructor() {
        super();
        this.state = {
            ads: null
        };
    }

    componentWillMount() {
        var cachedAds = localStorage.getItem("ads");

        if (!cachedAds) {
            localStorage.setItem("ads", JSON.stringify(ads));
            cachedAds = JSON.stringify(ads);
        }

        this.setState({ ads: JSON.parse(cachedAds) });
    }

    render() {
        return (
            <div className="ads">
                <h1>List of ads:</h1>

                <ul className="ads-list">
                    {
                        this.state.ads && this.state.ads.map((item, i) => (
                            <li className="ads-list-item" key={i}>
                                <Link className="link-item" to={`/details/${item.id}`}>
                                    <span>{item.title}</span>
                                    <i className="icon-eye"></i>
                                </Link>
                            </li>
                        ))
                    }
                </ul>

            </div>
        )
    }
}

export default ListAd;