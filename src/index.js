import React from 'react';
import {render} from 'react-dom';
import {HashRouter,
        Switch,
        Route} from 'react-router-dom';

import "./style.css";

import ListAd from './components/listAd/index';
import Header from './components/header/index';
import DetailsAd from './components/detailsAd/index';
import EditAd from './components/editAd/index';
import NotFound from './components/notFound/index';


const Router = () => (
    <Switch>
        <Route exact path='/' component={ListAd}/>
        <Route path='/new' component={EditAd}/>
        <Route path='/edit/:id' component={EditAd}/>
        <Route path='/details/:id' component={DetailsAd}/>
        <Route path='*' component={NotFound} />
    </Switch>
);

render((
    <HashRouter>
        <div className="app-container">
            <Header />
            <Router/>
        </div>
    </HashRouter>
), document.getElementById('app'));
